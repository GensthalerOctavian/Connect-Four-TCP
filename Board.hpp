#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <vector>
#include <iostream>

using namespace std;

#include "Token.hpp"

class Board : public sf::Drawable
{
    private:
        Token grid[6][7];
        unsigned int tokenRadius;
        unsigned int padding;
        sf::Color emptyColor;
        sf::Vector2i lastTaken;

    public:
        Board(int radius);
        Board() {};
        ~Board() {};

        void hoverCheck (sf::Vector2i mPos, sf::Color c);
        void takeTokenOnColumn (int column, sf::Color c);
        void takeToken (sf::Vector2i mPos, sf::Color c);
        void setLastTakenX(int x);
        void update();

        int getBoardPosX(sf::Vector2i mPos);

        bool horizontalVictoryCheck(sf::Color c);
        bool verticalVictoryCheck(sf::Color c);
        bool diagonalVictoryCheck(sf::Color c);

    private:
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};
