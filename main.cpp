#include <iostream>
#include "Game.hpp"

int main (int argc, char *argv[])
{
    if (argc != 3)
    {
        printf ("Sintaxa: %s <adresa_server> <port>\n", argv[0]);
        return -1;
    }

    Game game;

    game.setPort(atoi(argv[2]));
    game.connectTo(argv[1]);

    game.run();

    return 0;
}
