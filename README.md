# Proiect Tip B Connect Four 

Gensthaler Octavian, B5


# Cerinte
Aveti nevoie de ultima versiune de SFML (2.4) care poata fi obtinuta de [aici](https://www.sfml-dev.org/download/sfml/2.4.2/)

Un compilator C++11 e recomandat

# Instalare

Rulati "make all" o data in folderul de la baza, si a doua oara in folderul "Server".

# Ghid utilizare
1) Porniti Server/ConnectFourServer si lasati-l sa ruleze in fundal
2) In directorul principal, rulati "ConnectFour IP 2908"
3) Dati click pe un cerculet gol pentru a plasa un token, castigatorul este primul care poate plasa 4 tokenuri orizontale sau 4 tokenuri verticale, sau 4 in diagonala

# Bibliografie
* https://profs.info.uaic.ro/~computernetworks/files/NetEx/S12/ServerConcThread/servTcpConcTh2.c
* https://profs.info.uaic.ro/~computernetworks/files/NetEx/S12/ServerConcThread/cliTcpNr.c
* https://en.wikipedia.org/wiki/Connect_Four
* http://mathworld.wolfram.com/Connect-Four.html