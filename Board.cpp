#include "Board.hpp"
#include <iostream>

Board::Board(int radius)
{
    emptyColor = sf::Color(33, 49, 72);
    tokenRadius = radius;
    padding = 25;

    for(unsigned int i = 0; i < 6; i++)
    {
        for(unsigned int j = 0; j < 7; j++)
        {
            sf::Vector2f iPos(0.0f, 0.0f);

            iPos.y = i * (tokenRadius*2 + padding) + padding;
            iPos.x = j * (tokenRadius*2 + padding) + padding;

            Token t(i+1, j+1, emptyColor);

            t.getCircleShape() -> setPosition(iPos);
            t.getCircleShape() -> setRadius(tokenRadius);

            grid[i][j] = t;
        }
    }
}




void Board::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for(unsigned int i = 0; i < 6; i++)
        for(unsigned int j = 0; j < 7; j++)
            target.draw(grid[i][j]);
}


void Board::hoverCheck(sf::Vector2i mPos, sf::Color c)
{
    for(unsigned int i = 0; i < 6; i++)
        for(unsigned int j = 0; j < 7; j++)
            if(grid[i][j].isTaken() == false)
            {
                sf::FloatRect bounds = grid[i][j].getCircleShape() -> getGlobalBounds();

                if (bounds.contains(mPos.x, mPos.y))
                    grid[i][j].getCircleShape() -> setFillColor(c);
                else
                    grid[i][j].getCircleShape() -> setFillColor(emptyColor);
            }
}



void Board::takeToken(sf::Vector2i mPos, sf::Color c)
{
    for(unsigned int i = 0; i < 6; i++)
        for(unsigned int j = 0; j < 7; j++)
        {
            sf::FloatRect bounds = grid[i][j].getCircleShape() -> getGlobalBounds();

            if (bounds.contains(mPos.x, mPos.y) && grid[i][j].isTaken() == false)
            {
                grid[i][j].take(c);
            }
        }
}



void Board::takeTokenOnColumn(int column, sf::Color c)
{
    grid[0][column].take(c);
}


void Board::update()
{
    bool updatedOnce = false;

    for(unsigned int i = 0; i < 5; i++)
        for(unsigned int j = 0; j < 7; j++)
        {
            if (grid[i][j].isTaken() && grid[i+1][j].isTaken() == false)
            {
                sf::Color c = grid[i][j].getCircleShape() -> getFillColor();

                grid[i][j].free();
                grid[i+1][j].take(c);

                lastTaken.x = j;

                if(i+1 == 0)
                    lastTaken.y = 0;
                else
                    lastTaken.y = i+1;

                updatedOnce = true;
            }
        }

    if(!updatedOnce)
        lastTaken.y = 0;
}


int Board::getBoardPosX(sf::Vector2i mPos)
{
    for(unsigned int i = 0; i < 6; i++)
        for(unsigned int j = 0; j < 7; j++)
        {
            sf::FloatRect bounds = grid[i][j].getCircleShape() -> getGlobalBounds();

            if (bounds.contains(mPos.x, mPos.y) && grid[i][j].isTaken() == false)
                return j;
        }
}


bool Board::horizontalVictoryCheck(sf::Color c)
{
    sf::Color toCheck = c;

    int matching = 0;

    for(unsigned int i = 0; i < 6; i++)
        for(unsigned int j = 0; j < 7; j++)
        {
            if(matching >= 4)
                return true;

            if(grid[i][j].getCircleShape() -> getFillColor() == c)
                matching++;
            else
                matching = 0;
        }
            

    return false;
}


bool Board::verticalVictoryCheck(sf::Color c)
{
    sf::Color toCheck = c;

    int matching = 0;

    for(unsigned int i = 0; i < 7; i++)
        for(unsigned int j = 0; j < 6; j++)
        {
            if(matching >= 4)
                return true;

            if(grid[j][i].getCircleShape() -> getFillColor() == c)
                matching++;
            else
                matching = 0;
        }
            

    return false;
}


bool Board::diagonalVictoryCheck(sf::Color c)
{
    sf::Color toCheck = c;

    for(unsigned int i = 0; i < 7; i++)
        for(unsigned int j = 0; j < 4; j++)
        {
            if(grid[i][j].getCircleShape() -> getFillColor() == c &&
                grid[i+1][j+1].getCircleShape() -> getFillColor() == c &&
                grid[i+2][j+2].getCircleShape() -> getFillColor() == c &&
                grid[i+3][j+3].getCircleShape() -> getFillColor() == c)
                    return true;
        }

    for(unsigned int i = 6; i >= 3; i--)
        for(unsigned int j = 0; j < 3; j++)
        {
            if(grid[i][j].getCircleShape() -> getFillColor() == c &&
                grid[i-1][j-1].getCircleShape() -> getFillColor() == c &&
                grid[i-2][j-2].getCircleShape() -> getFillColor() == c &&
                grid[i-3][j-3].getCircleShape() -> getFillColor() == c)
                    return true;
        }

    return false;
}
