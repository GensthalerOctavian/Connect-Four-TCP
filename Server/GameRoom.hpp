#include <ctime>
#include <cstdlib>

class GameRoom
{
    private:
        int descriptors[2];
        int currentTurn;
        int firstPlayer;
        
    public:
        GameRoom();
        GameRoom(int p1, int p2);
        ~GameRoom();

        void addPlayer(int pid);
        int getPlayer(int index);
        int getCurrentTurn();
        int getFirstPlayer();
        void setFirstPlayer(int n);
        void setCurrentTurn(int p);
        int getSecondPlayer();
        int getOppositeOf(int p);
};
