#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <signal.h>
#include <pthread.h>
#include <iostream>
#include <vector>
#include <ctime>

#include "GameRoom.hpp"

using namespace std;

#define PORT 2908

extern int errno;

typedef struct thData{
	int idThread;   //id-ul thread-ului tinut in evidenta de acest program
	int cl;         //descriptorul intors de accept
    vector<GameRoom>* gameRooms;
    int gameRoomIndex;
}thData;

static void *treat(void *); /* functia executata de fiecare thread ce realizeaza comunicarea cu clientii */
int raspunde(void *);

int main ()
{
    struct sockaddr_in server;	// structura folosita de server
    struct sockaddr_in from;	
    int nr;		//mesajul primit de trimis la client 
    int sd;		//descriptorul de socket 
    int pid;
    pthread_t th[100];    //Identificatorii thread-urilor care se vor crea
	int i=0;
    int connectionCount = 0;

    vector<GameRoom> gameRooms;
    int lastGameRoom = 0;

    GameRoom g;
    gameRooms.push_back(g);

    /* crearea unui socket */
    if ((sd = socket (AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror ("[server]Eroare la socket().\n");
        return errno;
    }

    /* utilizarea optiunii SO_REUSEADDR */
    int on=1;
    setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
  
    /* pregatirea structurilor de date */
    bzero (&server, sizeof (server));
    bzero (&from, sizeof (from));
  
    /* umplem structura folosita de server */
    /* stabilirea familiei de socket-uri */
    server.sin_family = AF_INET;	
    
    /* acceptam orice adresa */
    server.sin_addr.s_addr = htonl (INADDR_ANY);
    
    /* utilizam un port utilizator */
    server.sin_port = htons (PORT);
  
    /* atasam socketul */
    if (bind (sd, (struct sockaddr *) &server, sizeof (struct sockaddr)) == -1)
    {
        perror ("[server]Eroare la bind().\n");
        return errno;
    }

    /* punem serverul sa asculte daca vin clienti sa se conecteze */
    if (listen (sd, 2) == -1)
    {
        perror ("[server]Eroare la listen().\n");
        return errno;
    }

    /* servim in mod concurent clientii...folosind thread-uri */
    while (1)
    {
        int client;
        thData * td; //parametru functia executata de thread     
        socklen_t length = sizeof (from);

        cout << "[SERVER] Asteptam la portul " << PORT << endl;

        //client= malloc(sizeof(int));
        // acceptam un client (stare blocanta pina la realizarea conexiunii) 
        if ( (client = accept (sd, (struct sockaddr *) &from, &length)) < 0)
        {
            perror ("[server]Eroare la accept().\n");
            continue;
        }
        
        // s-a realizat conexiunea, se astepta mesajul
        int idThread;   //id-ul threadului
        int cl;         //descriptorul intors de accept

        td=(struct thData*)malloc(sizeof(struct thData));	
        td -> idThread = i++;
        td -> cl = client;
        td -> gameRooms = &gameRooms;
        td -> gameRoomIndex = lastGameRoom;

        connectionCount++;

        gameRooms[lastGameRoom].addPlayer(td->cl);
        
        if (connectionCount % 2 == 0)
        {
            lastGameRoom++;

            GameRoom g2;
            gameRooms.push_back(g2);
        }
 
        for(int j = 0; j < gameRooms.size(); j++)
            cout << "Camera " << j << ": " << gameRooms[j].getPlayer(0) << " si " << gameRooms[j].getPlayer(1) << endl;

        pthread_create(&th[i], NULL, &treat, td);	      

        cout << endl;
	} 
};				

static void *treat(void * arg)
{		
    while(1)
    {
        struct thData tdL; 
        tdL= *((struct thData*)arg);	
        
        if(tdL.gameRooms -> at(tdL.gameRoomIndex).getPlayer(1) != 0 && tdL.gameRooms -> at(tdL.gameRoomIndex).getPlayer(0) != 0)
        {
            pthread_detach(pthread_self());		
            if(raspunde((struct thData*)arg) == -100)
                return NULL;
        }
    }

    return(NULL);	
};


int raspunde(void *arg)
{
    int nr, i=0, answer = 100;
	struct thData tdL; 
	tdL= *((struct thData*)arg);

    if(tdL.gameRooms -> at(tdL.gameRoomIndex).getCurrentTurn() == tdL.cl)
    {
        cout << "[THREAD " << tdL.idThread << "] Acum astept ceva de la client..." << endl;

        if (read (tdL.cl, &nr,sizeof(int)) <= 0)
        {
            printf("[THREAD %d]\n",tdL.idThread);
            perror ("Eroare la read() de la client.\n");
        }
        
        switch(nr)
        {
            case -100:
                cout << "[THREAD " << tdL.idThread << "] clientul s-a deconectat" << endl;
                answer = -200;
                close (tdL.cl);
                write (tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl), &answer, sizeof(int));
                close (tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl));
                break;

            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5: 
            case 6:
                cout << "[THREAD " << tdL.idThread << "] Am citit valoarea " << nr << " de la clientul " << tdL.cl << endl;
                cout << "[THREAD " << tdL.idThread << "] acum o trimitem la clientul " << tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl) << endl;
                write (tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl), &nr, sizeof(int));
                tdL.gameRooms -> at(tdL.gameRoomIndex).setCurrentTurn(tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl));
                break;

            case 1000:
            case 1001:
            case 1002:
            case 1003:
            case 1004:
            case 1005:
            case 1006:
                cout << "[THREAD " << tdL.idThread << "] Am citit valoarea " << nr << " de la clientul " << tdL.cl << endl;
                cout << "[THREAD " << tdL.idThread << "] acum o trimitem la clientul " << tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl) << endl;
                write (tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl), &nr, sizeof(int));
                tdL.gameRooms -> at(tdL.gameRoomIndex).setCurrentTurn(tdL.gameRooms -> at(tdL.gameRoomIndex).getOppositeOf(tdL.cl));
                cout << "A CASTIGAT CLIENTUL " << tdL.cl << endl;
                break;
        }
        
        cout << "[THREAD " << tdL.idThread << "] Mesajul a fost receptionat -> " << nr << endl;
    }
    else if(tdL.gameRooms -> at(tdL.gameRoomIndex).getCurrentTurn() == 0)
    {
        cout << "[THREAD " << tdL.idThread << "] se comunica primului jucator ca este primul." << endl;
        tdL.gameRooms -> at(tdL.gameRoomIndex).setCurrentTurn(tdL.gameRooms -> at(tdL.gameRoomIndex).getFirstPlayer());

        answer = 101;
        write (tdL.gameRooms -> at(tdL.gameRoomIndex).getFirstPlayer(), &answer, sizeof(int));
        answer = 102;
        write (tdL.gameRooms -> at(tdL.gameRoomIndex).getSecondPlayer(), &answer, sizeof(int));
    }

    return nr;
}
