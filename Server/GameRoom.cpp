#include "GameRoom.hpp"

GameRoom::GameRoom()
{
    descriptors[0] = 0;
    descriptors[1] = 0;

    currentTurn = 0;
}

GameRoom::~GameRoom()
{
}

GameRoom::GameRoom(int p1, int p2)
{
    descriptors[0] = p1;
    descriptors[1] = p2;
}


void GameRoom::addPlayer(int pid)
{
    if(descriptors[0] == 0)
        descriptors[0] = pid;
    else
    {
        srand(time(NULL));

        if(rand() % 2 == 1)
            firstPlayer = pid;
        else
            firstPlayer = descriptors[0];

        descriptors[1] = pid;
    }
}


int GameRoom::getPlayer(int index)
{
    return descriptors[index];
}

int GameRoom::getCurrentTurn()
{
    return currentTurn;
}


void GameRoom::setFirstPlayer(int n)
{
    firstPlayer = n;
}


void GameRoom::setCurrentTurn(int p)
{
    currentTurn = p;
}

int GameRoom::getFirstPlayer()
{
    return firstPlayer;
}

int GameRoom::getSecondPlayer()
{
    if(descriptors[0] == firstPlayer)
        return descriptors[1];
    else
        return descriptors[0];
}


int GameRoom::getOppositeOf(int p)
{
    if(descriptors[0] == p)
        return descriptors[1];
    else
        return descriptors[0];
}
